#include "header.h"
#include <pthread.h>
#include <stdlib.h>
#include <stdio.h>
#include<time.h>
 

int main()
{
  pthread_t* thread;  
  int i;
  int ans;
  printf("For matrix-matrix multiplication, press 1. For matrix-vector multiplication, press 2.\n");
  scanf("%d",&ans);

  if(ans==1){
  create_matrix(A);
  create_matrix(B);
  thread = (pthread_t*) malloc(num*sizeof(pthread_t));
  
  for (i = 0; i < num; i++)
  {
    if (pthread_create (&thread[i], NULL, multmatrix, (void*)i) != 0 )
    {
      perror("Cannot create thread");
      free(thread);
      exit(-1);
    }
  }
  for (i = 0; i < num; i++)
  pthread_join (thread[i], NULL);
  free(thread);
  printf("Product Calculated\n");
  }
 
  if(ans==2){
  create_matrix(A);
  create_vector(D);
  thread = (pthread_t*) malloc(num*sizeof(pthread_t));
  
  for (i = 0; i < num; i++)
  {
    if (pthread_create (&thread[i], NULL, multmatrixvector, (void*)i) != 0 )
    {
      perror("Cannot create thread");
      free(thread);
      exit(-1);
    }
  }
  for (i = 0; i < num; i++)
  pthread_join (thread[i], NULL);
  free(thread);
  printf("Product Calculated\n");
	}
  return 0;
}
