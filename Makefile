OBJS = main.o create.o multmatrix.o multmatrixvector.o

prog1 : $(OBJS)
	gcc -o prog1 $(OBJS) -pthread

main.o : main.c header.h
	gcc -c main.c

create.o : create.c header.h
	   gcc -c create.c

multmatrix.o : multmatrix.c header.h
		gcc -c multmatrix.c

multmatrixvector.o : multmatrixvector.c header.h
		     gcc -c multmatrixvector.c

clean:
	rm $(OBJS)
